# Main CSS docs

## Breakpoints

### Breakpoint up

Usage:

```scss
// No media query necessary for `xs` breakpoint as it's effectively `@media (min-width: 0) { ... }`
@include media-breakpoint-up(sm) { ... }
@include media-breakpoint-up(md) { ... }
@include media-breakpoint-up(lg) { ... }
@include media-breakpoint-up(xl) { ... }

// Custom
@include media-breakpoint-up(500px) { ... }

// Example: Hide starting at `min-width: 0`, and then show at the `sm` breakpoint
.custom-class {
  display: none;
}
@include media-breakpoint-up(sm) {
  .custom-class {
    display: block;
  }
}
```

Output:

```scss
// xs: Extra small devices (portrait phones, less than 576px)
// No media query for `xs` since this is the default

// sm: Small devices (landscape phones, 576px and up)
@media (min-width: 576px) { ... }

// md: Medium devices (tablets, 768px and up)
@media (min-width: 768px) { ... }

// lg: Large devices (desktops, 992px and up)
@media (min-width: 992px) { ... }

// xl: Extra large devices (large desktops, 1200px and up)
@media (min-width: 1200px) { ... }

// Custom 500px
@media (min-width: 500px) { ... }
```

### Breakpoint down

Usage:

```scss
@include media-breakpoint-down(xs) { ... }
@include media-breakpoint-down(sm) { ... }
@include media-breakpoint-down(md) { ... }
@include media-breakpoint-down(lg) { ... }
// No media query necessary for `xl` breakpoint as it has no upper bound on its width

// Custom
@include media-breakpoint-down(500px) { ... }

// Example: Style from medium breakpoint and down
@include media-breakpoint-down(md) {
  .custom-class {
    display: block;
  }
}
```

Output:

```scss
// xs: Extra small devices (portrait phones, less than 576px)
@media (max-width: 575.98px) { ... }

// sm: Small devices (landscape phones, less than 768px)
@media (max-width: 767.98px) { ... }

// md: Medium devices (tablets, less than 992px)
@media (max-width: 991.98px) { ... }

// lg: Large devices (desktops, less than 1200px)
@media (max-width: 1199.98px) { ... }

// xl: Extra large devices (large desktops)
// No media query since the extra-large breakpoint has no upper bound on its width

// Custom 500px
@media (max-width: 499.98px) { ... }
```

### Breakpoint between

Usage:

`@include media-breakpoint-between(min, max)`

```scss
@include media-breakpoint-between(xs, sm) { ... } // only xs
@include media-breakpoint-between(sm, md) { ... } // only sm
@include media-breakpoint-between(md, lg) { ... } // only md
@include media-breakpoint-between(lg, xl) { ... } // only lg
// only xl: use @include media-breakpoint-up(xl) { ... }

// Example multiple breakpoint
@include media-breakpoint-between(md, xl) { ... } // md up to xl

// Custom
@include media-breakpoint-between(500px, 800px) { ... }
```

Output:

```scss
// (xs, sm): Extra small devices (portrait phones, less than 576px)
@media (max-width: 575.98px) { ... }

// (sm, md): Small devices (landscape phones, 576px and up)
@media (min-width: 576px) and (max-width: 767.98px) { ... }

// (md, lg): Medium devices (tablets, 768px and up)
@media (min-width: 768px) and (max-width: 991.98px) { ... }

// (lg, xl): Large devices (desktops, 992px and up)
@media (min-width: 992px) and (max-width: 1199.98px) { ... }

// Example multiple breakpoint (md, xl): Apply styles starting from medium devices and up to extra large devices
@media (min-width: 768px) and (max-width: 1199.98px) { ... }

// Custom (500px, 800px)
@media (min-width: 500px) and (max-width: 799.98px) { ... }
```

Ignored:

```scss
// min = max
@include media-breakpoint-between(xs, xs) { ... }
@include media-breakpoint-between(500px, 500px) { ... }

// max < min
@include media-breakpoint-between(sm, xs) { ... }
@include media-breakpoint-between(800px, 500px) { ... }
```

## Grid

### Containers

| &nbsp; | Extra small (<576px) | Small (≥576px) | Medium (≥768px) | Large (≥992px) | Extra large (≥1200px) |
| --- | --- | --- | --- | --- | --- |
| `.container` width | None (auto) | 540px | 720px | 960px | 1140px |
| `.container-fluid` width | 100% | 100% | 100% | 100% | 100% |

```html
<div class="container">
  <!-- Content here -->
</div>

<div class="container-fluid">
  <!-- Content here -->
</div>
```

### Basic columns

| &nbsp; | Extra small (<576px) | Small (≥576px) | Medium (≥768px) | Large (≥992px) | Extra large (≥1200px) |
| --- | --- | --- | --- | --- | --- |
| Base classes | `.col-{1-12}` | `.col-sm-{1-12}` | `.col-md-{1-12}` | `.col-lg-{1-12}` | `.col-xl-{1-12}` |
| Order classes | `.order-{1-12}` | `.order-sm-{1-12}` | `.order-md-{1-12}` | `.order-lg-{1-12}` | `.order-xl-{1-12}` |

| # of columns | Gutter width | Nestable | Column ordering |
| --- | --- | --- | ---|
| 12 | 30px (15px on each side of a column) | Yes | Yes |

```html
<!-- Basic -->
<div class="container">
  <div class="row">
    <div class="col-4">
      One of three columns
    </div>
    <div class="col-4">
      One of three columns
    </div>
    <div class="col-4">
      One of three columns
    </div>
  </div>
</div>

<!-- Ordering -->
<div class="container">
  <div class="row">
    <div class="col-4">
      First, but unordered
    </div>
    <div class="col-4 order-12">
      Second, but last
    </div>
    <div class="col-4 order-1">
      Third, but first
    </div>
  </div>
</div>
```

### Auto-layout columns

| &nbsp; | Extra small (<576px) | Small (≥576px) | Medium (≥768px) | Large (≥992px) | Extra large (≥1200px) |
| --- | --- | --- | --- | --- | --- |
| Equal-width classes | `.col` | `.col-sm` | `.col-md` | `.col-lg` | `.col-xl` |
| Width content classes | `.col-auto` | `.col-sm-auto` | `.col-md-auto` | `.col-lg-auto` | `.col-xl-auto` |

```html
<!-- Equal-width -->
<div class="container">
  <div class="row">
    <div class="col">
      1 of 3
    </div>
    <div class="col">
      2 of 3
    </div>
    <div class="col">
      3 of 3
    </div>
  </div>
</div>

<!-- Setting one column width -->
<div class="container">
  <div class="row">
    <div class="col">
      1 of 3
    </div>
    <div class="col-6">
      2 of 3 (wider)
    </div>
    <div class="col">
      3 of 3
    </div>
  </div>
</div>

<!-- Width content -->
<div class="container">
  <div class="row">
    <div class="col">
      1 of 3
    </div>
    <div class="col-auto">
      2 of 3 (width content)
    </div>
    <div class="col">
      3 of 3
    </div>
  </div>
</div>
```
