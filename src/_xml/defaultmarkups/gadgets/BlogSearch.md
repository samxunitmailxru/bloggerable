# Blog Search

Let visitors search your blog.

## Usage

```html
<b:widget id='BlogSearch1' locked='false' title='Search This Blog' type='BlogSearch' visible='true'/>
```
