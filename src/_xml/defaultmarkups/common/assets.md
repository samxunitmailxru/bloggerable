# Assets

## Main CSS

Usage:

Included in `src/theme.xml`.

```html
<b:include cond='!data:view.isLayoutMode' data='{ name: "css_main" }' name='assets'/>
```

## Main JavaScript

Usage:

Included in `src/theme.xml`.

```html
<b:include data='{ name: "js_main" }' name='assets'/>
```
